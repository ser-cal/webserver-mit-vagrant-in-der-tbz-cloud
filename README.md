[10]: https://github.com
[20]: https://web.microsoftstream.com/video/663c03fd-5562-42f0-855f-fc98e7769c3e


# Webserver auf TBZ-Cloud mit Vagrant deklarativ aufsetzen

Ziel dieses Projektes (Tutorials) ist es, dass die Lernenden **Vagrant** verstehen und praktisch anwenden können. Bei dieser Arbeit handelt es sich um ein klassisches "Hands-on"-Projekt. Ziel ist es, Schritt für Schritt ein deklaratives Setup für eine Webserver-Testumgebung aufzubauen, die zum Entwickeln genutzt - und in kürzester Zeit ortsunabhängig und mit wenig Aufwand - nachgebaut werden kann. Sämtliche Daten sind persistent, bleiben also auch beim Zerstören der VM bestehen und können jederzeit im Sinne von CI/CD "idempotent" wieder automatisch auf einer neuen/anderen Umgebung verwendet werden.<br>


## Tutorial und Cookbook

Ergänzend zu diesem Repository habe ich noch folgendes Tutorial erstellt mit einem Cookbook (Klartext mit Kommandos). Geeignet zum mitmachen.

**Tutorial** - Webserver mit Vagrant in der TBZ-Cloud aufsetzen

![Video1:](../../x_gitressourcen/Video.png) 15min
[![Tutorial](images/titel_200.png)](https://tbzedu-my.sharepoint.com/:v:/r/personal/marcello_calisto_tbz_ch/Documents/Webserver%20mit%20Vagrant%20in%20der%20TBZ-Cloud.mp4?csf=1&web=1&e=e9mNfJ)


![Cookbook](webserver-auf-tbz-cloud.txt)


## Inhaltsverzeichnis

[TOC]


## Voraussetzungen:
- [Vagrant](https://www.vagrantup.com/) installiert
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads) und Extension Pack (gleiche Version) installiert <br>_(Es kann auch ein anderer Provider wie z.B. VMware benutzt werden - Default ist Virtualbox)_
- [Gitlab](https://gitlab.com/) Account
- Windows: [GitBash](https://git-scm.com/downloads) auf dem lokalen Host installiert
- Mac oder Linux: (Bash bereits vorhanden, sonst ebenfalls auf [GitBash](https://git-scm.com/downloads)) verfügbar
- Editor: z.B: [Visual Studio Code](https://code.visualstudio.com/) , [Atom](https://atom.io/) oder [Sublime Text](https://www.sublimetext.com/) etc...

<br>


## Vagrant: Sinn und Zweck
Vagrant ist sehr gut geeignet, um schnell und unkompliziert Testumgebungen aufzubauen und, falls der Zweck erfüllt ist, diese auch wieder genau so schnell und unkompliziert zu löschen. In diesem Tutorial wirst Du ein **generelles Verständnis über die Funktionsweise von Vagrant** erhalten und auch schon die ersten "Hands-on"-Übungen durchführen. Falls Du nicht weiter weisst, hilft Dir **[Vagrant-Docs][20]** in allen Belangen weiter. Sämtliche Kommandos sind mit passenden Erklärungen versehen und für Anfänger und auch Fortgeschrittene eine sehr gute Quelle.


## Das folgende Dokument ist wie folgt strukturiert:
1. Im **ersten Abschnitt** **[Vagrant Einstieg](#vagrant-einstieg)** machen wir uns einwenig mit "Vagrant" vertraut. Wir installieren mit einem ersten einfachen deklarativen Script (Vagrantfile) eine Ubuntu-VM und setzen dabei gleich die ersten Vagrant-Kommandos "Hands-on" ein. Nach Abschluss dieses Kapitels sollten Dir die gängigsten Vagrant-Befehle vertraut sein, und Du hast erfolgreich eine erste Vagrantbox aufgesetzt, zum Laufen gebracht und wieder "zerstört" (gelöscht)

2. Im **zweiten Abschnitt** **[NGINX-Webserver deklarativ aufsetzen](#nginx-webserver-deklarativ-aufsetzen)** gehts bereits ein erstes Mal zur Sache. Wir setzen Schritt für Schritt einen NGINX-Webserver auf - und zwar so, dass diese Umgebung jederzeit gelöscht und in kürzester Zeit wieder erstellt werden kann. Auch auf einer anderen Umgebung. Der Webseitencontent wird somit persistiert und es kann auch nach einem "Destroy" und einer Neuinstallation wieder auf den früher erstellten Content zugegriffen werden. 

      **Wichtig:** <br>
      In diesem Tutorial sollen die deklarativen Scripts selber (von Hand, mit Editor) erstellt werden.
      Wer alles durcharbeitet, sollte zum Ziel kommen.<br>
      Das Repository beinhaltet am Ende folgende Files und Ordner:
        ![Screenshot](images/000-repo-webserver.png) 

---

## Vagrant Einstieg

### In die TBZ-Cloud VM wechseln

Du erhältst vorgängig folgende Angaben, um Dich mit einer Dir zugewiesenen TBZ-Cloud VM zu verbinden:
- Link zum Download für den VPN-Client (Wireguard)
- IP-Adresse Deiner VM.
- Private-Key.
- Template für die VPN-Verbindung zur TBZ-Cloud.
- Anleitung zum Einrichten des VPN-Clients
 (Konfiguration mit Deiner persönlichen IP-Adresse und Deinem Private-Key).

Wenn alles soweit eingerichtet ist, baust du wie folgt eine SSH-Verbindung zur TBZ-Cloud VM auf:

> `$ pwd ` _Checken, in welchem Verzeichnis du bist_<br>
> `$ ssh ubntu@<ip>  ` _Hier die IP-Adresse deiner VM eintragen_ <br>
  ![Screenshot](images/001-jump-tbz-cloud.png) 


### Preflight Checks

Bevor wir loslegen, zuerst checken, ob Vagrant, Virtualbox und SSH installiert ist. Wir verwenden hier die "Gitbash" auf Windows. Es spielt im Moment noch keine Rolle, in welchem Verzeichnis wir uns befinden. 

> `$ vagrant -v ` _Checken, welche Vagrant-Version installiert ist_<br>
> `$ vboxmanage -v  ` _checken, welche Virtualbox-Version installiert ist_ <br>
> `$ ssh  ` _Checken, ob SSH installiert ist_<br>
  ![Screenshot](images/002-sw-check.png) 



### Setup erstes Projekt (Ubuntu-VM)
Mit folgenden Schritten das Verzeichnis für die erste mit Vagrant erstellte Ubuntu-VM vorbereiten

> `$ cd <Projekt-Mutterverzeichnis> ` _ins Mutterverzeichnis des vorgesehenen Projektes wechseln_<br>
> `$ pwd  ` _kontrolle, ob im richtigen Verzeichnis_ <br>
> `$ mkdir ubuntu  ` _Projektverzeichnis "ubuntu" anlegen_ <br> 
> `$ cd ubuntu  ` _in's Verzeichnis "ubuntu" wechseln_
  ![Screenshot](images/003-gitbash-ubuntu-verz.png)


Im aktuellen Verzeichnis ein Vagrantfile (für das OS Ubuntu Precise32) erstellen und überprüfen, ob das Vagrantfile vorhanden ist:
> `$ vagrant init hashicorp/precise32 `<br>
> `$ ls -ali `
  ![Screenshot](images/004-vagrantfile.png)

Config-file mit Editor öffnen und ckecken
> `$ vim Vagrantfile ` _Inhalt anschauen_<br>
  ![Screenshot](images/005-vagrantfile-inhalt.png)
<br>

### VM starten und überprüfen
Wenn soweit alles ok ist, können wir die VM wie folgt zum ersten Mal starten 
> `$ vagrant up ` _Virtualbox-VM mit Vagrant starten_<br>
  ![Screenshot](images/006-vagrant-up.png)
<br>

In die VM "hüpfen" und überprüfen
> `$ vagrant ssh ` _in die Ubuntu-VM "hüpfen"_<br>
> `$ uname -a  ` _Checken, ob Distro stimmt --> Ubuntu_ <br>
> `$ df -h ` _Diskfree Human-readable_ <br>
  ![Screenshot](images/007-ssh-auf-Ubuntu-vm.png)

VM vom Host aus überprüfen
> `$ exit ` _aus der VM zurück auf den Host_<br>
> `$ vboxmanage list runningvms  ` _checken, welche Virtualbox-VMs am Laufen sind_ <br>
  ![Screenshot](images/008-gitbash-vbox-ubuntu.png)


### Files zwischen Host und VM sharen
„Synced Folders“ Verzeichnisse und Files können zwischen dem Host-System und den VM’s (Guests) ge’shared‘ werden.

Hier ein Beispiel: Auf dem Host (TBZ Haupt-VM) das File "hello.txt" erstellen, eine Zeile Random-Text eintragen und überprüfen.
> `$ pwd ` _Schauen, in welchem Verzeichnis_<br>
> `$ touch hello.txt ` _File "hello.txt" erstellen_<br>
> `$ echo "irgendeinentext" >> hello.txt ` _beliebigen Text ins **hello.txt** schreiben_<br>
> `$ cat hello.txt ` _Inhalt des Files **hello.txt** anschauen_<br>
  ![Screenshot](images/009-txt-file.png)

Um dieses File im Gastsystem zu sehen, muss ich wie folgt vorgehen 
> `$ vagrant ssh ` _auf die Ubuntu-VM "hüpfen"_<br>
> `$ cd /vagrant ` _dieses Verzeichnis ist mit dem Gastsystem ge'sync'ed_<br>
> `$ ls -al ` _hier ist das vorher erstellte **hello.txt** ersichtlich_<br>
> `$ cat hello.txt ` _Inhalt des Files **hello.txt** anschauen_<br>
  ![Screenshot](images/010-txt-file.png)

### Nützliche Befehle

**VM mit "halt" runterfahren** (abschalten)
```  
$ vagrant halt
```
**VM mit "up" starten**
```  
$ vagrant up
```
...gem. folgendem Screenshot

> `$ cd <Projektverzeichnis> ` _ins richtige Directory wechseln_<br>
> `$ vagrant halt ` _VM runterfahren_<br>
> `$ vagrant up ` _VM starten_<br>
  ![Screenshot](images/011-vm-runter-hoch.png)
<br>


**VM mit "suspend" anhalten/einfrieren.** <br>Free up Memory und CPU. (Z.B. falls ich knapp an Ressourcen bin, weil z.B. noch andere VMs laufen)
```  
$ vagrant suspend
```
**VM mit "resume" wieder reaktivieren.**<br>Geht schneller, als wenn VM frisch gestartet wird
```  
$ vagrant resume
```
Anwendung

> `$ cd <Projektverzeichnis> ` _ins richtige Directory wechseln_<br>
> `$ vagrant suspend ` _VM anhalten / einfrieren_<br>
> `$ vagrant resume ` _VM reaktivieren_<br>
  ![Screenshot](images/012-vm-suspend-resume.png)
<br>

**Sämtliche VBox-Eingabemöglichkeiten auflisten** (Virtualbox-Befehl)
```  
$ VBox
```
**Überprüfen, welche VMs gerade laufen** (Virtualbox-Befehl)
```  
$ VBoxManage.exe list vms
```
Anwendung
> `$ VBox ` _Zeigt sämtliche VBox-Abfrageoptionen_<br>
> `$ VBoxManage.exe list vms ` _Zeigt, welche VMs aktuell gestartet sind_<br>
  ![Screenshot](images/013-vms-auflisten.png)
<br>

**VM löschen** (zerstören)<br>Kann beliebig angewendet werden. Einer der grössten Vorteile von Vagrant. Genau diese VM kann später **mit nur einem Kommando** (vagrant up) wieder genau gleich hergestellt werden, wie vor dem Löschen (alle notwendigen Schritte dazu sind im Vagrantfile deklariert)
```  
$ vagrant destroy (-f)
```
**Überprüfen, welche VMs gerade laufen** (Virtualbox-Befehl)
```  
$ VBoxManage.exe list vms
```
Anwendung
> `$ vagrant destroy (-f)` _VM komplett löschen (falls nötig mit Parameter **-f** )_<br>
> `$ VBoxManage.exe list vms ` _kontrollieren, ob nicht mehr vorhanden_<br>
  ![Screenshot](images/014-vms-zerstoeren-pruefen.png)
<br>

**VM wieder neu erstellen**
Probe auf's Exempel. Wir erstellen im Projektordner dieselbe VM neu mit folgendem Befehl.
```  
$ vagrant up
```
Anwendung <br>

Dieselbe VM, die vorher zerstört wurde, ist innerhalb von wenigen Minuten wieder neu erstellt. Unten ein Screenshot der Windows-Verzeichnisse. Das Ganze kann auch noch wie folgt in der VM überprüft werden:
> `$ vagrant ssh` _in die VM "hüpfen"_<br>
> `$ cd /vagrant ` _das file "hello.txt" sollte wieder ersichtlich sein_<br>
  ![Screenshot](images/015-filecheck.png)


**Vagrant Hilfe** (Help)<br>
Hilfe zu sämtlichen Vagrant-Kommandos kann wie folgt bezogen werden

```  
$ vagrant -h
$ vagrant --help
```
...gem. folgendem Screenshot
  ![Screenshot](images/016-vagrant-help.png)
<br>

...oder eine Stufe tiefer zu bestimmten Parametern (z.B. zu "vagrant up")
```  
$ vagrant up -h
```
...gem. folgendem Screenshot
  ![Screenshot](images/017-vagrant-up-help.png)
<br>

Beim Kommando `$ vagrant status` kann zusätzlich noch der Log-Level definiert werden (untersch. Outputs). Das ist nützlich, wenn Probleme auftreten und diese mit den aktuellen Settings nicht erkennbar sind.<br> Wir unterscheiden zwischen folgenden Status:
 - debug
 - info (normal)
 - warn
 - error

Status wie folgt überprüfen und bei Bedarf anders setzen:
> `$ vagrant status`  _Defaultwert ist "Info"_<br>
> `$ export VAGRANT_LOG=debug ` _ändern, um mehr Infos zu erhalten_<br>
  ![Screenshot](images/018-vagrant-status-debug.png)

Es ist aber auch möglich, den Status der Variable zu ändern, ohne Variable fix zu setzen; und zwar wie folgt:
> `$ vagrant status --debug ` _nur 1x, nicht persistent_

- - - 

# NGINX Webserver deklarativ aufsetzen
In diesem Abschnitt werden wir nun einen NGINX-Webserver mit Vagrant deklarativ aufbauen. Dazu benötigen wir das bereits bekannte Vagrantfile und zusätzlich ein Provision-Shellscript. Dieses Shellscript wird benötigt, um auf der bereitgestellten VM weitere Installationen und Konfigurationen durchzuführen. Damit diese Umgebung jederzeit und ortsunabhängig nachgebaut werden kann, nutzen wir Github als "Distributed Version Control System". Wir veröffentlichen den gesamten Code hier in diesem Repository. 

**Vorbereitung**<br>
Zuerst erstellen wir für den NGINX-Webserver ein neues Verzeichnis

> `$ cd <Projektverzeichnis> ` _ins richtige Directory wechseln_<br>
> `$ mkdir nginx ` _Verzeichnis erstellen_<br>
> `$ cd nginx ` _Ins Verzeichnis wechseln_<br>
  ![Screenshot](images/019-nginx-umgebung.png)
<br>

**Projekt initialisieren / OS (Vagrant Box) ergänzen**<br>
In diesem Fall initialisieren wir das Vagrant-Projekt mit dem Minimal-Flag, um die vielen auskommentierten Zeilen im Vagrantfile rauszulöschen und später eigene Inhalte einzufügen. Zusätzlich muss noch ein OS (in Vagrant "Box" genannt) angegeben werden. Wir entscheiden uns für die bereits früher verwendete Ubuntu-Box. Da wir diese ge'downloaded' haben, ist sie bereits lokal vorhanden. Andernfalls muss sie im Vagrant-Repository geholt werden (dauert dann etwas länger)
> `$ vagrant init ubuntu/xenial64 --minimal ` _Initialisieren mit der Ubuntu-Box_<br>
  ![Screenshot](images/020-nginx-vagrantfile-small.png)
<br>

**Vagrantfile ergänzen**<br>
Bevor die VM zum erstem Mal gestartet wird, ergänzen wir das Config-File noch mit einem Eintrag. Dazu öffnen wir das Vagrantfile mit einem beliebigen Editor (in diesem Fall mit **vim**) und geben der VM den Namen "web-dev". Dieser VM-Name ist frei wählbar und erscheint später, wenn wir in die VM "hüpfen", auch im Prompt (siehe übernächstes Bild)
  ![Screenshot](images/021-nginx-vagrantfile-small.png)
<br>

**VM zum ersten Mal starten und checken**
> `$ vagrant up ` _VM starten_<br>
  ![Screenshot](images/022-nginx-vagrant-up.png)

...anschliessend in die VM "hüpfen" und überprüfen, ob der im Vagrantfile definierte Systemname angezeigt wird
> `$ vagrant ssh ` _in die VM "hüpfen"_<br>
  ![Screenshot](images/023-nginix-vagrant-ssh.png)


**Provision-Script erstellen für weitere SW-Installationen**<br>
Jetzt haben wir zwar ein funktionsfähiges Ubuntu am laufen, aber noch keine zusätzliche Software darin. Damit wir diese deklarativ installieren können, müssen wir ein separates Provision-Skript erstellen. Dieses soll anschliessend automatisch ausgeführt werden und muss deshalb explizit im Vagrantfile definiert werden. In unserem Fall erstellen wir dafür das **`provision.sh`**

**Provision-Script in Vagrantfile einbinden**<br>
Die folgenden Screenshots zeigen, welcher Eintrag dazu im Vagrantfile ergänzt werden muss und wo das **`provision.sh`** abgelegt wird. Damit wird sichergestellt, dass **nach** der OS-Installation weitere SW/Applikationen automatisiert installiert und konfiguriert werden kann. Dieses Script kann auch zu einem späteren Zeitpunkt ergänzt oder angepasst werden. 
> `$ vi Vagrantfile ` _Vagrantfile öffnen und in der zweitletzten Zeile das provision.sh einbinden"_<br>
  ![Screenshot](images/024-nginx-provision-sh.png)

**Provision-Script erstellen**<br>
Nun noch das File **provision.sh**, auf welches im Vagrantfile verwiesen wird, erstellen und mit Anweisungen füllen.

Zuerst prüfen, ob wir im richtigen Verzeichnis sind (dort wo auch das Vagrantfile ist). Dann das File **provision.sh** erstellen

> `$ hostname ` _auf Host-System_<br>
> `$ pwd ` _im gleichen Verzeichnis wie das Vagrantfile_<br>
> `$ touch provision.sh ` _"provision.sh" erstellen_<br>
   ![Screenshot](images/025-nginx-provision-sh.png)

**Provision-Script ergänzen**<br>
Jetzt muss das **`provision.sh`** noch mit Inhalt gefüllt werden. Wir wollen folgende drei Punkte darin festhalten:

```
1. Sämtliche Package-Libraries updaten 
2. NGINX-Packet installieren
3. NGINX starten
```

...die entsprechenden Einträge sehen wie folgt aus:
   ![Screenshot](images/026-nginx-provision-sh.png)


**Provisioning aktivieren**<br>
Wir haben nun das Vagrantfile ergänzt und ein zusätzliches Provisioning-Script erstellt. Um beide Änderungen wirksam zu machen, müssen wir verstehen, wie Vagrant in einem solchen Fall funktioniert.

Versuchen wir mal, die VM zu rebooten:
> `$ vagrant reload ` _VM rebooten_<br>
  ![Screenshot](images/027-nginx-vagrant-reload.png)



Wir sehen, dass das geänderte Vagrantfile eingelesen wurde. Der Eintrag am Ende zeigt allerdings, dass das **`provision.sh`**-Script nicht ausgeführt werden kann.<br>

**Wichtiger Hinweis:** <br>
Vagrant provisioniert standardmässig **nur beim ersten** "`vagrant init`". Das ist in unserem Fall bereits geschehen. Die Meldung ganz am Ende hilft uns dabei und zeigt auch gleich auf, was zu tun ist. Wir müssen das Kommando"`vagrant provision`" nachliefern - als Alternative auch mit dem Flag `--provision` möglich. Erst dann wird das Provision-Script **`provision.sh`** angekickt und somit die Befehle darin ausgeführt.

Hier noch der passende Screenshot dazu. In der letzten Zeile sieht man auch, dass die Installation von NGINX gestartet wurde.
> `$ vagrant provision ` _VM neu provisionieren_<br>
  ![Screenshot](images/028-nginx-vagrant-provision-1.png)
  ![Screenshot](images/029-nginx-vagrant-provision-2.png)

**Überprüfen, ob NGINX installiert wurde:**<br>
Um zu überprüfen, ob die Installation und Provisioning erfolgreich durchgeführt wurden, können wir in die VM wechseln und den Status von NGINX wie folgt abfragen
> `$ vagrant ssh ` _in die VM "hüpfen"_<br>
> `$ service nginx status ` _checken, ob NGINX läuft_
  ![Screenshot](images/030-nginx-vagrant-ssh-nginx-status.png)

In der VM kontrollieren, ob die `index.html`-Seite angezeigt wird:
> `$ wget -qO- localhost ` _HTML-Code im Klartext ausgeben_<br>
  ![Screenshot](images/031-nginx-webseiten-text.png)

### Zugriff auf den Webserver via Browser

Damit der Webserver auch von aussen zugänglich gemacht wird, brauchen wir noch ein **"Portforwarding"**. Standardmässig läuft NGINX wie andere Webserver auch, auf **Port 80**. Damit vom Host-Webbrowser auf den Webserver zugegriffen werden kann, muss der Port des Gastsystems an den Host weitergeleitet werden. Wir entscheiden uns in diesem Fall für den **Host-Port 8080**. 

Solche **allgemeine** VM-Konfigurationen werden ebenfalls im **Vagrantfile** eingetragen - in unserem Fall gleich unterhalb des Pfades zum Provision-Script
   ![Screenshot](images/032-nginx-port-forwarding-hostseitig-1.png)

Bei der anschliessenden Überprüfung auf dem Host-Browser erkennen wir nun aber, dass der Zugriff auf die index.html des Gastsystems (noch) nicht funktioniert

> `localhost:8080` _Localhost oder Hosts-IP plus definierten Host-Port in beliebigen Browser eingeben_
  ![Screenshot](images/033-nginx-port-forwarding-hostseitig.png)

...es erscheint die Fehlermeldung, dass **keine** Verbindung zum Webserver aufgebaut werden kann

Der Grund, weshalb unsere Verbindung noch nicht funkioniert, ist einmal mehr im Config-file zu suchen. Wir haben zwar den Portforwarding-Eintrag im Vagrantfile gespeichert, aber dieses wurde seither noch nicht aktiviert.
**Wichtig:** Um das abgeänderte Vagrantfile zu aktivieren, reicht ein **Reboot** (oder hier "Reload" genannt). Der Parameter **`--provision`** muss in diesem Fall **NICHT** ausgeführt werden. Das wäre nur nötig, wenn das verlinkte Shellscript (**provision.sh**) abgeändert worden wäre. Das ist hier allerdings **NICHT** der Fall.<br>

Hier der Beweis :
> `$ vagrant reload ` _VM rebooten - Portforwarding **80** auf **8080** wird gesetzt_<br>
  ![Screenshot](images/034-nginx-port-forwarding-hostseitig.png)

_...erneut_ `localhost:8080` _in beliebigen Browser eingeben... und - **es funktioniert**_
 ![Screenshot](images/035-nginx-port-forwarding-hostseitig.png)


### NGINX Configuration-File anpassen, um persistente Nutzung zu ermöglichen

**Was funktioniert bis jetzt**<br>
Es ist soweit alles so vorbereitet, dass der NGINX Web-Dienst jederzeit und ortsunabhängig aufgesetzt werden kann. Der aktuelle Stand ermöglicht es uns, die Umgebung sehr schnell hochzufahren und danach mit einem Webbrowser über `localhost:8080` auf den Webserver zuzugreifen.

**Was funktioniert bis jetzt NOCH NICHT**<br>
Falls wir Änderungen innerhalb der VM (z.B. im Web-content) durchführen, wären diese **nach** einem `$ vagrant destroy` / `$ vagrant up` für immer und ewig **gelöscht**. Wir müssen in einem nächsten Schritt also sicherstellen, dass unsere Testumgebung Änderungen festhalten (persistieren) kann - sonst ist das Setup einer solchen Entwicklungsumgebung eigentlich zwecklos.  

Folgende Schritte sind notwendig, um dieses Problem zu lösen:

  1.  Configuration-File für den NGINX-Webdienst analysieren
  2.  Content "persistent" machen
  3.  Synced Folders konfigurieren
  4.  Testing

<br>

---

**1. Configuration-File für den NGINX-Webdienst analysieren**<br>
Wenn in einer Linux-Umgebung ein Dienst startet, wird als erstes das zugehörige Config-file durchgearbeitet. Dieses befindet sich standardmässig im Verzeichnis **/etc**.

Das Config-file des NGINX-Webdienstes befindet sich auf Ubuntu im Verzeichnis **/etc/nginx/sites-enabled** und lautet **default**. In der folgenden Sequenz "hüpfen" wir in die VM und schauen uns die Standard-Configuration dieses Files mal an. 
 
In diesem File ist der Pfad zum Web-Content eingetragen. Diesen Pfad brauchen wir, um die Webinhalte auf einen "shared folder" zu kopieren und anschliessend mit einem symbolischen Link dahin zu verknüpfen. Bei einer Abfrage werden dann die Inhalte des "Shared folders" angezeigt. Damit dieser Vorgang beim Setup persistent ist, müssen noch ein paar Dinge berücksichtigt werden... 

Umsetzung:
> `$ vagrant ssh ` _in die VM "hüpfen"_<br>
  ![Screenshot](images/036-ssh-login.png)

> `$ ls /etc/nginx/sites-enabled/default ` _Config-file von NGINX_<br>
> `$ head -38 /etc/nginx/sites-enabled/default ` _die ersten 38 Zeilen ausgeben_<br>
  ![Screenshot](images/037-www-content.png)

...im orangen Rahmen sehen wir, dass sämtlicher Web-Content des NGINX-Webservers in folgendem Verzeichnis abgelegt wird:

```
/var/www/html
```
Mit folgenden Kommandos können wir kurz überprüfen, ob das index.html auch wirklich in diesem Verzeichnis liegt
> `$ ls /var/www/html ` _Files im Verzeichnis abrufen_<br>
> `$ cat /var/www/html/index.nginx-debian.html ` _html-Code des Indexfiles ausgeben_<br>
  ![Screenshot](images/038-index-content.png)

<br>

---

**2. Content "persistent" machen**<br>
Damit unser Web-Content persistent und unabhängig von der VM entwickelt werden kann, müssen wir einmalig sämtliche Files im Verzeichnis `/var/www/html` in ein Unterverzeichnis des **Shared-Folders** kopieren. Dieses Unterverzeichnis nennen wir zwecks Nachvollziehbarkeit `www`. Die Daten im Quellenverzeichnis `/var/www/html` können nach dem kopieren gelöscht werden. Denn päter werden diese beiden Aktionen in das `provision.sh` geschrieben, damit diese Schritte bei einer Neuinstallation (vagrant destroy, vagrant up) jeweils automatisch durchgeführt werden.<br> 
**Zur Erinnerung:** Der **Shared-Folder** (dort wo auch das Vagrantfile liegt), ist auf der VM unter `/vagrant` eingehängt. Der absolute Pfad des neuen, persistenten Verzeichnisses, muss also neu `/vagrant/www` lauten. 

Um sicher zu sein, am besten auf dem **Hostsystem** (im Hauptverzeichnis, dort wo das Vagrantfile liegt) kurz überprüfen, ob das **www**-Verzeichnis schon existiert. Falls nicht, dieses wie folgt erstellen:

> `$ mkdir www ` _im /vagrant-Verzeichnis das Verzeichnis **www** erstellen_<br>


**Vorgehen**

Zuerst die Inhalte des Verzeichnisses `/var/www/html`-File in den späteren **Shared-Folder** `/vagrant/www` kopieren. Danach den gesamten Inhalt des Quellenverzeichnisses `/var/www/html` löschen.

> `$ cp -r /var/www/html/* /vagrant/www ` _Originaler Webcontent in den "Shared-Folder" kopieren_<br>
> `$ sudo rm -rf /var/www/html ` _...und dann im Originalverzeichnis löschen_<br>
 ![Screenshot](images/039_webcontent.png)

Um das ganze noch besser zu visualisieren, ersetzen wir das Standardfile `index.nginx-debian.html` mit einem einfachen `index.html`-File, in welchem wir einfach die Hintergrundfarbe abändern können. Hier der Code für das File `/vagrant/www/index.html` zum nachbauen.

```
<html>
<head>
<title>Welcome to the Cloud Native Bootcamp</title>
</head>
<body bgcolor="blue" text="black">
<center><h1>Welcome to the Cloud Native Bootcamp!</h1></center>
</body>
</html>
```

Anschliessend einen symbolischen Link darauf erstellen, so dass Anfragen direkt auf unseren **Shared-Folder** weitergeleitet werden.

> `$ sudo ln -s /vagrant/www /var/www/html ` _Symbolsichen Link erstellen_<br>
> `$ ls -al /var/www/html ` _Link checken_<br>
 ![Screenshot](images/040_symlink.png)


**Achtung:** Da das Verzeichnis `/var/www/html` bei einer Neuinstallation wieder installiert wird, müssen wir diese Befehle auch noch in das **Provision.sh**-Script eintragen (siehe weiter unten). So stellen wir sicher, dass wir auch nach einem `vagrant destroy` und anschliessendem `vagrant up` wieder auf die aktuellen **Shared-Folder**-Files zugreifen.

Es folgen deshalb nun noch die Anpassung des files **provision.sh**

> `$ rm -rf /var/www/html ` _Altes Verzeichnis inkl. Content löschen_<br>
> `$ ln -s /vagrant/www /var/www/html ` _Symbolischen Link des "alten" Verzeichnisses auf den persistenten "Shared-Folder" legen_<br>
  ![Screenshot](images/041-provision.png)

<br>

---

**3. Synced Folders konfigurieren**<br>
Wie bereits mehrmals erwähnt, gibt es ein „shared“ Verzeichnis. Dieses heisst per Default **/vagrant** und wird z.B. beim Starten der VM eingebunden (siehe zweites Bild, oranger Rahmen):

![Screenshot](images/042-vagrantup.png)

![Screenshot](images/043-vagrantup.png)

Der Alias „Synced Folder“ kann umkonfiguriert werden. Im Moment wird in diesem Fall das gesamte Verzeichnis von `/home/ubuntu/nginx` synchronisiert. Wir benötigen aber aktuell lediglich das Unterverzeichnis `www` für unsere NGINX-Umgebung. In der folgenden Sequenz werden wir das „Vagrantfile“ so anpassen, dass nur noch dieses Unterverzeichnis ge’shared‘ ist.

**Reminder!** <br>
Wenn das „Vagrantfile“ verändert wird, muss die VM **nicht** zerstört werden! Anders ist das, wenn das File `provision.sh` modifiziert wird. Dann muss entweder die VM „destroyed“ und neu geladen-, oder gleich anschliessend das Kommando „vagrant provision“ ausgeführt werden.

Nach Änderungen im „Vagrantfile“ werden diese also gleich nach `vagrant up` eingelesen und aktiviert.

Mit folgenden Eingaben im Vagrantfile "disablen" wir zuerst `/vagrant` und enablen anschliessend die beiden Unterverzeichnisse `www` und `sites-enabled`

```
config.vm.synced_folder ".", "/vagrant", disabled: true
config.vm.synced_folder "www", "/vagrant/www"


  ...danach "speichern" nicht vergessen
```
Inhalt des **Vagrantfiles** mit folgenden Einträgen ergänzen
> `$ vi Vagrantfile ` _Shared-Folder einträge siehe im Bild_<br>
  ![Screenshot](images/044_synced-folder.png)

Danach VM reloaden, um die Änderungen im Vagrantfile zu aktivieren: 

> `$ vagrant reload ` _VM rebooten_<br>
 ![Screenshot](images/045_vagrantup.png)

  _...Shared Untererzeichnisse werden neu gemountet_
  ![Screenshot](images/046_synced-folder.png)


Auf dem Output oben sieht man nochmals gut, dass das Provisioning erst anschliessend durchgeführt wird. Diese gemounteten „Synced-Folders“ können also auch für das Provisioning, das erst später folgt, genutzt werden. 

---

**4. Testing**<br>
Nun geht es darum, nochmals alles zu überprüfen. Es macht demzufolge Sinn, dass die gesamte VM zerstört - und anschliessend neu installiert wird. Dazu benötigen wir lediglich zwei Kommandos und max. ein 5 Minuten Zeit.

Vorher ändern wir aber in der `index.html` die Hintergrundfarbe auf **Blue**. Dann sehen wir auch gleich, ob die Änderungen im "Synched Folder" auch gleich greifen. 

```
<body bgcolor="blue" text="black">
```
`$ vim /vagrant/www/index.html ` _Hintergrundfarbe im index.html ändern_<br>
 ![Screenshot](images/047-blue1.png)

> `$ vagrant halt ` _Guest-VM anhalten_<br>
> `$ vagrant destroy -f ` _Guest-VM zerstören_<br>
> `$ vagrant up ` _Guest-VM neu installieren_<br>
 ![Screenshot](images/048-guest-new.png)

Nachdem die VM wieder hochgekommen ist, auf einem Browser `localhost:8080` eingeben

 ![Screenshot](images/049-blue.png)

Wenn die Seite wie oben mit einem **blauen Hintergrund** erscheint, hat **alles** funktioniert.

Checken wir nun noch, ob das **index.html** im **Shared-Folder** während dem laufenden Betrieb verändert werden kann.
Setzen wir dazu die Hintergrundfarbe auf **Rot**.

```
<body bgcolor="red" text="black">
```
`$ vim /vagrant/www/index.html ` _Hintergrundfarbe im index.html ändern und speichern_<br>
 ![Screenshot](images/050-red1.png)

Webseite reloaden...

 ![Screenshot](images/051-red.png)

Wenn die Seite wie oben mit einem **roten Hintergrund** erscheint, hat **alles** funktioniert.


## Review
Wir haben erfolgreich mit einem **deklarativen Script**  einen Webserver aufgesetzt und können den Code/Content beliebig weiterentwickeln, persistent und unabhängig von der VM abspeichern und diese jederzeit verlustfrei zerstören und zu einem späteren Zeitpunkt **ohne Aufwand** wieder neu erstellen.  

# Herzlichen Glückwunsch


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>

- - -

- Autor: Marcello Calisto
- Mail: marcello.calisto@tbz.ch
